#!/usr/bin/env python3

import sys
import getopt
import os
import hashlib


def dirtreewalk(argv):
    try:
        optlist, arguments = getopt.getopt(argv, "h", ["help"])
    except getopt.GetoptError as error:
        print(str(error))
        sys.exit(2)

    if not optlist and not arguments:
        scriptname = os.path.basename(__file__)
        print("""usage: {}
            
positional argument:
    path        path to show
   
   
optional arguments:
    -h, --help  show this help message and exit
        """.format(scriptname))
        sys.exit(2)

    for o, a in optlist:
        if o in ("-h", "--help"):
            print("-h, --help  show this help message and exit")
            sys.exit(1)

    try:
        path = arguments[0]
        if not os.path.isdir(path):
            raise ValueError("Path not found: {}".format(path))

        walk_dir(path)

    except Exception as error:
        print("Error: {}".format(error))
        sys.exit(2)


def walk_dir(path):
    for element in os.listdir(path):
        try:
            subpath = os.path.join(path, element)

            if not os.access(subpath, os.R_OK):
                raise IOError("Insufficient Permission: {}".format(subpath))

            if os.path.isdir(subpath):
                print("{}   <dir>".format(element))
                walk_dir(subpath)

            else:
                md5 = hashlib.md5(open(subpath, 'rb').read()).hexdigest()
                print("{} {} {}".format(element, subpath, md5))

        except Exception as error:
            print("Error: {}".format(error))


if __name__ == "__main__":
    dirtreewalk(sys.argv[1:])
